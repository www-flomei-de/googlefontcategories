**"The missing piece" aka Font Classification for Google Webfonts**

While the [Google Font API](https://developers.google.com/fonts/docs/developer_api) delivers lots of information, it lacks information regarding the classification ("Sans Serif", "Display" etc.) of a font.  

This project regularly takes a look at the [Google Fonts repository](https://github.com/google/fonts/) and checks the `METADATA.pb` file for each font it finds. This file contains information on the classification of the font, "the missing piece" in the Font API. That information is then pushed into this repository and ready for further use.  

*Note:* When you find a font without a classification, there was no metadata file present. This happens for language-specific subsets of fonts, like "opensanshebrew". You should have a look at the parent font for information on the classification.